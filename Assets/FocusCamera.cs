﻿using UnityEngine;
using System.Collections;

public class FocusCamera : MonoBehaviour {
	PlayerManager pManager;
	// Use this for initialization
	void Start () {
		pManager = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerManager> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (pManager.CurrentInteractiveObject == null)
		{
			gameObject.camera.enabled = false;
			return;
		}
		transform.position = pManager.GetComponentInChildren<Camera> ().transform.position;
		transform.Translate (new Vector3 (0, 0.5f, 0));
		gameObject.camera.enabled = true;
		transform.LookAt (pManager.CurrentInteractiveObject.transform);
	}
}
