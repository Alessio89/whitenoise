﻿using UnityEngine;
using System.Collections;

public class DollyZoom : MonoBehaviour {

	public GameObject focus;
	public TextAsset Message_StartGame;
	GameObject player;
	public GameObject GFManager;

	// Use this for initialization
	void Start () {
		float distance = Vector3.Distance( transform.position, focus.transform.position);
		player = GameObject.FindGameObjectWithTag ("Player");
		initHeight = (float)FrustumHeightAtDistance(distance);
		player.GetComponent<PlayerManager> ().DisablePlayer ();
			
	}

	double FrustumHeightAtDistance(float distance) {
		return 2.0 * distance * Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad);
	}

	double FOVForHeightAndDistance(float height, float distance) {
		return 2 * Mathf.Atan(height * 0.5f / distance) * Mathf.Rad2Deg;
	}
	float initHeight;
	bool done = false;
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) && !done)
		{
			done = true;
			iTween.ValueTo(Camera.main.gameObject, iTween.Hash("from", Camera.main.fieldOfView, "to", 60, "time", 6, "easetype", "easeInOutSine", "onupdate", "UpdateFOV", "onupdatetarget", gameObject, "oncomplete", "EnablePlayer", "oncompletetarget", gameObject));
			iTween.MoveBy(Camera.main.gameObject, iTween.Hash("amount", new Vector3(0,-0.4f,6.2f), "time", 5.7f, "easetype", "easeInOutSine"));
		}

	}

	void EnablePlayer()
	{
		string[] message = Message_StartGame.text.Split (new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
		Dialogue.ShowMessage (message);
		Dialogue.managerObject.OnMessageEnd = () => {
			GFManager.GetComponent<GameflowManager>().S1_StartTimer( 30, () => {
				Dialogue.ShowMessage( new string[] { "Signor White, deve prendere un numero e attendere il suo turno!" } );

			});
		};
		Dialogue.AddActionOnTextAppear ("Ma dove", () => {
			iTween.RotateBy(player, iTween.Hash("amount", new Vector3(0, -0.2f, 0), "time", 11, "easeType", "easeInOutBack"));		
		});

		Dialogue.AddActionOnTextAppear ("Mmmh.", () => {
			iTween.RotateBy(player, iTween.Hash("amount", new Vector3(0, -0.1f, 0), "time", 2, "easeType", "easeInOutSine"));		
			
		});



		//GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerManager> ().EnablePlayer ();
		player.GetComponent<PlayerManager> ().originalY = Camera.main.transform.position.y;
		player.GetComponent<PlayerManager> ().EnablePlayer ();
	}

	void UpdateFOV(float fov)
	{
		Camera.main.fieldOfView = fov;
	}
}
