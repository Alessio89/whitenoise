﻿using UnityEngine;
using System.Collections;

public class QuadGui : MonoBehaviour {
	GUIText text;
	GameObject player;
	string[] Message;
	int index = 0;
	bool active = true;
	// Use this for initialization
	void Start () {
		Message = new string[6] { "Benvenuto nella demo del prototipo del gioco che andremo a creare.",
								  "Siamo giunti alla versione 0.0.3h e vi vado ad elencare le cose che dovrete testare in questa versione.",
								  "Una di queste cose è questo messaggio iniziale con le varie informazioni sulla versione corrente.\n Almeno sapremo sempre cosa c'è di nuovo e cosa invece non è cambiato. Ora iniziamo:",
			                      "Nuova Meccanica: Focus sugli oggetti 'importanti'. \nQuando ci si avvicina ad un oggetto che sarà importante all'avanzamento della trama o del\n gioco, apparirà in alto a destra una seconda telecamera \n che punterà all'oggetto in questione. Al momento è attiva per il quadro\n vicino al tavolo, tanto per far vedere come puo venire.",
			                      "Nuova Meccanica: Esaminare gli oggetti da più vicino.\n Se provate ad esaminare la libreria, noterete che la telecamera si sposterà e inquadrerà\n una serie di libri. L'intenzione è che alcuni oggetti \n possano essere nascosti in altri oggetti e andranno cercati usando\n un sistema simile.",
								  "Per ora è tutto. Buon testing :) - Alessio."
		};

		player = GameObject.FindGameObjectWithTag ("Player");
		//player.GetComponent<PlayerManager> ().DisablePlayer ();
		text = GameObject.Find ("[S] QuadText").guiText;
		text.text = Message[0];
		active = false;
		index = 0;
		text.text = "";
		renderer.enabled = false;
			GameObject.Find("[S] QuadText2").GetComponent<GUIText>().text = "";
		
		return;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape) && active)
		{
			active = false;
			index = 0;
			text.text = "";
			GameObject.Find("[S] QuadText2").GetComponent<GUIText>().text = "";
			renderer.enabled = false;
			player.GetComponent<PlayerManager>().EnablePlayer();
		}
		if (Input.GetKeyDown(KeyCode.Space) && active)
		{
			index++;
			if (index >= Message.Length)
			{
				active = false;
				index = 0;
				text.text = "";
				renderer.enabled = false;
				player.GetComponent<PlayerManager>().EnablePlayer();
				GameObject.Find("[S] QuadText2").GetComponent<GUIText>().text = "";
			}
			else
				text.text = Message[index];
		}
	}
}
