﻿using UnityEngine;
using System.Collections;

public class FocusWhenNearby : MonoBehaviour {
	GameObject player;
	public float InteractionRadius = 6;
	public float ActivationRadius = 6;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {

		if (PlayerIsNearby())
		{
			if (player.GetComponent<PlayerManager>().CurrentInteractiveObject == null)
			{
				player.GetComponent<PlayerManager>().CurrentInteractiveObject = gameObject;
			}
		}
		else
		{
			if (player.GetComponent<PlayerManager>().CurrentInteractiveObject != null && 
			    player.GetComponent<PlayerManager>().CurrentInteractiveObject.name == gameObject.name)
			{
				player.GetComponent<PlayerManager>().CurrentInteractiveObject = null;
			}
		}
	
	}
	protected bool PlayerIsNearby()
	{
		return (Vector3.Distance (player.transform.position, transform.position) < InteractionRadius);
	}
	
	protected bool PlayerCanReachMe()
	{
		return (Vector3.Distance (player.transform.position, transform.position) < ActivationRadius);
	}
}
