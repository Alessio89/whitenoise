﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Libreria : BaseInteractiveObject {

	Vector3 lastCameraPos;
	bool inUse = false;
	Camera c;
	protected override void OnUse ()
	{

		if (!inUse)
		{
			IntText.Disable();
			//iTween.MoveTo( Camera.main.gameObject, iTween.Hash( "position", new Vector3(100,100,100))   );
			//iTween.MoveBy( Camera.main.gameObject, iTween.Hash( "amount", new Vector3(10, 10, 10)));
			player.GetComponent<PlayerManager> ().DisablePlayer ();
			Transform cameraPos = (from o in GetComponentsInChildren<Transform> () where o.gameObject.name == "[S] CameraPos" select o).First ();
			c = cameraPos.gameObject.GetComponentInChildren<Camera> ();
			c.transform.position = Camera.main.transform.position;
			//c.transform.localEulerAngles = Camera.main.transform.localEulerAngles;
			StartCoroutine(DisableMain());
			iTween.MoveTo( c.gameObject, iTween.Hash( "position", cameraPos, 
			                                         "easeType", "easeInOutSine",
			                                         "oncomplete", "ShowText",
			                                         "oncompletetarget", gameObject));
			                                           
			inUse = true;
		}
	}

	void ShowText()
	{
		TextAsset t = Resources.Load ("BF Resources/Messages/Message_Libreria") as TextAsset;
		string[] message = t.text.Split (new string[] { "\n" }, System.StringSplitOptions.None);
		Dialogue.AddActionOnTextAppear ("lettura.", () => {
						canDismiss = true; });
		Dialogue.ShowMessage (message, 0.03f, false);

	}

	IEnumerator DisableMain()
	{
		yield return new WaitForSeconds (0.1f);
		c.enabled = true;
		
	}

	void EnablePlayer()
	{
		player.GetComponent<PlayerManager> ().EnablePlayer ();
		c.gameObject.GetComponentInChildren<Camera> ().enabled = false;
		
	}

	bool canDismiss = false;

	protected override void UpdateLogic ()
	{
		base.UpdateLogic ();
		if (Input.GetKeyDown(KeyCode.Space) && canDismiss)
		{

			Transform cameraPos = (from o in GetComponentsInChildren<Transform> () where o.gameObject.name == "[S] CameraPos" select o).First ();
			//cameraPos.gameObject.GetComponentInChildren<Camera> ().enabled = false;
			iTween.MoveTo( c.gameObject, iTween.Hash( "position", Camera.main.transform.position, 
			                                         "easeType", "easeInOutSine",
			                                         "oncomplete", "EnablePlayer",
			                                         "oncompletetarget", gameObject));

			inUse = false;
			canDismiss = false;
			IntText.Enable();

		}
	}
}
