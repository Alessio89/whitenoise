﻿using UnityEngine;
using System.Collections;

public class IntroGameManager : MonoBehaviour {

	public TextAsset TextFile;
	public TextAsset CreditFile;
	public GameObject GuiTextObject;
	public GUITexture Pictures;

	int phase = 1;

	string[] lines;
	int index = 0;
	// Use this for initialization
	void Start () {
		lines = TextFile.text.Split (new string[] { "\n" }, System.StringSplitOptions.None);
		//GuiTextObject.guiText.color = new Color (1, 1, 1, 1);
		GuiTextObject.guiText.font.material.color = new Color (1, 1, 1, 0);
		GuiTextObject.guiText.text = lines [0];
		iTween.FadeTo (GuiTextObject, iTween.Hash ("amount", 1, "time", 1.5f, "easetype", "Linear", "oncomplete", "FadeOut", "oncompletetarget", gameObject));
	}

	void FadeOut()
	{
		iTween.FadeTo (GuiTextObject, iTween.Hash ("amount", 0, "time", 1, "easetype", "Linear", "oncomplete", "NextText", "oncompletetarget", gameObject, "delay", 1.5f));
	}

	void NextText()
	{
		index++;
		if (index >= lines.Length)
		{
			index = 0;
			if (phase == 1)
			{
				Pictures.enabled = true;
				iTween.FadeTo (Pictures.gameObject, iTween.Hash ("amount", 1, "time", 0.1f, "easetype", "linear"));
				iTween.ColorTo ( GameObject.Find("Quad"), iTween.Hash("color", new Color(1,1,1,1), "time", 0.5f, "easeType", "easeOutSine", "oncomplete", "PhaseTwo", "oncompletetarget", gameObject));
				phase = 2;

			}
			else
			{
				iTween.ColorTo ( GameObject.Find("Quad"), iTween.Hash("color", new Color(0,0,0,1), "time", 1.5f, "easeType", "easeOutSine", "oncomplete", "LoadScene", "oncompletetarget", gameObject));
			}
			return;
		}
		GuiTextObject.guiText.text = lines [index];
		iTween.FadeTo (GuiTextObject, iTween.Hash ("amount", 2, "time", 1.5f, "easetype", "Linear", "oncomplete", "FadeOut", "oncompletetarget", gameObject, "delay", 1));
		
	}

	void LoadScene()
	{
		Application.LoadLevel( "Sala" );
	}

	void PhaseTwo()
	{

		GuiTextObject.guiText.font.material.color = new Color (0, 0, 0, 1);
		GuiTextObject.guiText.color = new Color (0, 0, 0, 1);
		lines = CreditFile.text.Split (new string[] { "\n" }, System.StringSplitOptions.None);
		GuiTextObject.guiText.text = lines [0];
		iTween.FadeTo (GuiTextObject, iTween.Hash ("amount", 1, "time", 1.5f, "easetype", "Linear", "oncomplete", "FadeOut", "oncompletetarget", gameObject));
		
	}
		
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (phase == 1)
			{
				phase = 2;
				iTween.FadeTo (Pictures.gameObject, iTween.Hash ("amount", 1, "time", 0.1f, "easetype", "linear"));
				iTween.ColorTo ( GameObject.Find("Quad"), iTween.Hash("color", new Color(1,1,1,1), "time", 0.5f, "easeType", "easeOutSine", "oncomplete", "PhaseTwo", "oncompletetarget", gameObject));
				GuiTextObject.guiText.color = new Color(0,0,0,1);
				GuiTextObject.guiText.text = "";
			}
			else
			{
				iTween.ColorTo ( GameObject.Find("Quad"), iTween.Hash("color", new Color(0,0,0,1), "time", 1.5f, "easeType", "easeOutSine", "oncomplete", "LoadScene", "oncompletetarget", gameObject));
				GuiTextObject.guiText.text = "";
				
			}
		}
	}
}
