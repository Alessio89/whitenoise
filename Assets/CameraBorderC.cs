﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraBorderC : MonoBehaviour {

	public GUISkin Skin;

	// Use this for initialization
	void Start () {
	
	}

	void OnGUI()
	{
		if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>().CurrentInteractiveObject == null)
			return;
		if (Skin != null)
			GUI.skin = Skin;
		Camera cam = transform.camera;
		GUI.Box(new Rect(cam.pixelRect.x, (Screen.height - cam.pixelRect.yMax), cam.pixelWidth, cam.pixelHeight), "") ;
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
