﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GameflowManager : MonoBehaviour {

	// Variabili relative alla Sala (scena 1)
	float S1_Timer = 0;
	float S1_Delay = 30;
	public bool S1_TimerIsRunning = false;
	public System.Action S1_OnTimerEnds;

	public bool S1_EverythingRead = false;

	// Use this for initialization
	void Start () {
	
	}

	public void S1_StartTimer(float delay, System.Action onEnd = null)
	{
		S1_Timer = 0;
		S1_Delay = delay;
		S1_OnTimerEnds = onEnd;
		S1_TimerIsRunning = true;
	}

	public void S1_StopTimer()
	{
		S1_TimerIsRunning = false;
		S1_OnTimerEnds = null;
		S1_Timer = 0;
	}

	// Update is called once per frame
	void Update () {
		if (S1_TimerIsRunning)
		{
			S1_Timer += Time.deltaTime;
			if (S1_Timer > S1_Delay)
			{
				if ( S1_OnTimerEnds != null )
					S1_OnTimerEnds();
				S1_Timer = 0;
				S1_TimerIsRunning = false;
			}
		}

		if (S1_EverythingRead)
			return;

		S1_EverythingRead = true;

		foreach(BaseMessageObject o in (from go in GameObject.FindObjectsOfType(typeof( BaseMessageObject)) select go as BaseMessageObject))
		{
			if (!o.UsedAtLeastOnce)
			{
				S1_EverythingRead = false;
				break;
			}
		}

		if (S1_EverythingRead)
		{
			GameObject.Find("Giornale").renderer.enabled = true;
		}


	}
}
