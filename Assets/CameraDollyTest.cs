﻿using UnityEngine;
using System.Collections;

public class CameraDollyTest : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>().DisablePlayer();
	
	}
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space))
		{
			iTween.ValueTo( Camera.main.gameObject, iTween.Hash("from", 90, "to", 60, "time", 2, "onupdate", "UpdateFOV", "onupdatetarget", gameObject));
		}
	}
	
	void UpdateFOV(float fov)
	{
		Camera.main.fieldOfView = fov;
		Debug.Log ("test");
	}
}
