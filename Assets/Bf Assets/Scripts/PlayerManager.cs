﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	public bool UsingObject = false;
	public bool Enabled = true;
	Vector3 lastPos;
	public float originalY;
	float originalX;

	public GameObject Arms;

	public GameObject CurrentInteractiveObject = null;
	
	// Use this for initialization
	void Start () {
		Screen.lockCursor = true;
		lastPos = transform.position;
		originalY = Camera.main.transform.position.y;
		//GetComponent<CharacterController> ().enabled = false;
	}
	bool changeAnimation = true;
	bool right = true;
	// Update is called once per frame
	void ChangeDirection()
	{
		changeAnimation = true;
	}

	void ReturnToCenter()
	{
		if (right)
		{
			iTween.MoveBy(Camera.main.gameObject, iTween.Hash("amount",new Vector3(0f, -0.01f, 0), 
			                                                  "easeType", "easeInOutSine", 
			                                                  "time", 0.6f,
			                                                  "oncomplete", "ChangeDirection",
			                                                  "Space", Space.Self,
			                                                  "oncompletetarget", gameObject));
			right = false;

		}
		else
		{
			iTween.MoveBy(Camera.main.gameObject, iTween.Hash("amount",new Vector3(0f, -0.1f, 0), 
			                                                  "easeType", "easeInOutSine", 
			                                                  "time", 0.6f,
			                                                  "oncomplete", "ChangeDirection",
			                                                  "Space", Space.Self,
			                                                  "oncompletetarget", gameObject));
			right = true;

		}
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Return))
		{
			GameObject.Find("Braccia").animation["Grab"].speed = 1;
			GameObject.Find("Braccia").animation.Play("Grab");
		}
	}

	void FixedUpdate () {
		
		if (Dialogue.IsMessageShowing())
		{
			iTween.Stop(Camera.main.gameObject);
			Debug.Log("Test test");
			return;
		}
		if (!Enabled)
			return;


		if (transform.position != lastPos)
		{
			GameObject.Find("Braccia").animation.Play("Walk");
			iTween.MoveBy(Camera.main.gameObject, iTween.Hash("amount",new Vector3(0, 0.06f, 0), 
			                                                  "easeType", "Linear", 
			                                                  "time", 0.3f,
			                                                  "Space", Space.Self,
			                                                  "looptype", "pingpong",
			                                                  "oncompletetarget", gameObject));

		}
		else
		{
			GameObject.Find("Braccia").animation.Stop();
			iTween.Stop(Camera.main.gameObject);
			Camera.main.transform.position = new Vector3( Camera.main.transform.position.x, originalY, Camera.main.transform.position.z);
			changeAnimation = true;
		}
		lastPos = transform.position;
	}



	public void TurnHead(bool right, float time)
	{
		iTween.RotateTo (gameObject, iTween.Hash ("rotation", transform.localEulerAngles + new Vector3(0, (right) ? 45 : -45, 0), "easetype", "easeInOutSine", "time", time));
	}

	public void Kneel()
	{
		iTween.MoveBy (Camera.main.gameObject, iTween.Hash ("amount", -transform.up , "easetype", "easeInOutSine", "time", 1));
	}

	public void EnablePlayer()
	{
		GetComponent<MouseLook> ().enabled = true;
		GetComponent<CharacterMotor> ().enabled = true;
		Camera.main.GetComponent<MouseLook> ().enabled = true;
		Enabled = true;
	}

	public void EnableMouseLook()
	{
		GetComponent<MouseLook> ().enabled = true;
	}

	public void DisablePlayer()
	{
		iTween.Stop (Camera.main.gameObject);
		GetComponent<MouseLook> ().enabled = false;
		//GetComponent<CharacterController> ().enabled = false;
		GetComponent<CharacterMotor> ().enabled = false;
		Enabled = false;
		Camera.main.GetComponent<MouseLook> ().enabled = false;
	}
}
