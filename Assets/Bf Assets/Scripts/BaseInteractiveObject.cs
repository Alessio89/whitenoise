﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class BaseInteractiveObject : MonoBehaviour {

	public delegate void MyDelegate ();

	// The player gameobject reference
	protected GameObject player;
	
	// The reference to the 3D text object

	// new
	public InteractiveText IntText;
	protected bool FadedIn = false;
	protected bool FadedOut = false;
	//

	public string ObjectName = "Oggetto";
	
	// The radius at which an item becomes interactive
	public float InteractionRadius = 4;
	public float ActivationRadius = 2;
	
	// The time it takes for the text to fade
	public float FadingTime = 1.5f;

	public Color textColor = Color.white;
	public Color actionTextColor = Color.white;

	// The action that will be displayed on screen
	public string Action = "Esamina";
	public string Prefix = "[ ";
	public string Suffix = "]";
	public KeyCode InteractionKey = KeyCode.F;

	// Store the state of the object (selected\not selected)
	protected bool Selected = false;

	// Is the text already instantiated?
	protected bool Instantiated = false;

	// Is the text over the object enabled?
	protected bool enableText = true;

	// The y offset of the action text
	public float ActionTextOffset = -0.2f;

	public bool CameraFocusOnObject = false;

	// Used to test for ray intersection. Using this instead of a name comparison ensure that two objects with the
	// same name can exist in the same scene.
	[HideInInspector]
	public string UniqueID = "";

	[HideInInspector]
	public GameObject LinkedGameObject;

	[HideInInspector]
	public float DestroyDelay;
	//public List<System.Action> OnUse = new List<System.Action> ();

	public Vector3 TextOffset = Vector3.zero;
	
	// Initializing all the essential variables. This should not be overridden by the inherited classes
	void Start () {

		UniqueID = gameObject.name + "_INTOBJID";

		IntText = TextManager.Create (UniqueID, gameObject, ObjectName, true);
		
		if (GameObject.FindGameObjectWithTag(TextManager.InteractiveTextTag) == null)
		{
			Debug.LogError("Impossibile tovare un oggetto con il tag del testo");
		}

		player = GameObject.FindGameObjectWithTag ("Player");
	
		if (TextOffset == Vector3.zero)
		{
			TextOffset = new Vector3( 0, renderer.bounds.size.y + 0.1f, 0);
		}

		IntText.TextOffset = TextOffset;
		IntText.TextColor = textColor;
		IntText.gameObject.renderer.enabled = false;

		IntText.ActionText.Text.text = Prefix + InteractionKey.ToString() + " - " + Action + Suffix;
		IntText.ActionTextOffset = ActionTextOffset;

		Initialize ();

		Debug.Log ("ID:" + UniqueID);
	}

	protected virtual void Initialize()
	{
	}
	
	// Update is called once per frame
	void Update () {
		UpdateLogic ();	
	}


	protected virtual void UpdateLogic()
	{
		// Check if the player is in the activation radius
		if (PlayerIsNearby())
		{
			if (player.GetComponent<PlayerManager>().CurrentInteractiveObject == null && CameraFocusOnObject)
			{
				player.GetComponent<PlayerManager>().CurrentInteractiveObject = gameObject;
			}
			// Test
			//Camera.main.transform.LookAt(transform);
			//
			if (!FadedIn)
			{
				if (IntText.Active)
				{
					IntText.FadeIn();	
					FadedIn = true;
				}
			}
			
			IntText.UpdateTextPosition();

			// Check if the player is looking at the object and that the object isn't already selected
			if (PlayerIsLookingAtObject() && !Selected && PlayerCanReachMe())
			{
				if (IntText.Active)
				{
					IntText.ActionText.FadeIn();
					Selected = true;
				}
			}
			// If instead the player is looking away from the object, and the object was previously selected
			else if ((!PlayerIsLookingAtObject() && Selected) || !PlayerCanReachMe())
			{
				IntText.ActionText.FadeOut();
				// We can safely assume the object is not selected anymore
				Selected = false;
			}

			if (Selected && player.GetComponent<PlayerManager>().Enabled)
			{
				if ( Input.GetKeyDown(InteractionKey))
				{
					OnUse ();
				}
			}
		}
		else // If player is not nearby
		{
			if (player.GetComponent<PlayerManager>().CurrentInteractiveObject != null && 
			    player.GetComponent<PlayerManager>().CurrentInteractiveObject.name == gameObject.name &&
			    CameraFocusOnObject)
			{
				player.GetComponent<PlayerManager>().CurrentInteractiveObject = null;
			}
			// Check to see if the object was near the player last frame
			if (FadedIn)
			{
				DisableText();
			}
			
			// We can now safely assume the object is not near the player anymore
			FadedIn = false;
		}	
	}
	
	protected void DisableText()
	{
		IntText.FadeOut ();
	}

	float offset = 30;
	Ray ray;
	Ray rayTopLeft;
	Ray rayTopRight;
	Ray rayBottomLeft;
	Ray rayBottomRight;
	protected bool PlayerIsLookingAtObject()
	{
		ray = Camera.main.ScreenPointToRay (new Vector3 (Screen.width / 2, Screen.height / 2, 0));
		rayTopLeft = Camera.main.ScreenPointToRay (new Vector3 ((Screen.width / 2) - offset, (Screen.height / 2) - offset, 0));
		rayTopRight = Camera.main.ScreenPointToRay (new Vector3 ((Screen.width / 2) + offset, (Screen.height / 2) - offset, 0));
		rayBottomLeft = Camera.main.ScreenPointToRay (new Vector3 ((Screen.width / 2) - offset, (Screen.height / 2) + offset, 0));
		rayBottomRight = Camera.main.ScreenPointToRay (new Vector3 ((Screen.width / 2) + offset, (Screen.height / 2) + offset, 0));
		RaycastHit hit;
		if (Physics.Raycast( ray, out hit ))
		{
			if (hit.collider.GetComponent<BaseInteractiveObject>() != null)
			{
				if (hit.collider.GetComponent<BaseInteractiveObject>().UniqueID == gameObject.GetComponent<BaseInteractiveObject>().UniqueID)
				{
					return true;
				}
			}
		}	
		else if (Physics.Raycast( rayTopLeft, out hit ))
		{
			if (hit.collider.GetComponent<BaseInteractiveObject>() != null)
			{
				if (hit.collider.GetComponent<BaseInteractiveObject>().UniqueID == gameObject.GetComponent<BaseInteractiveObject>().UniqueID)
				{
					return true;
				}
			}
		}
		else if (Physics.Raycast( rayTopRight, out hit ))
		{
			if (hit.collider.GetComponent<BaseInteractiveObject>() != null)
			{
				if (hit.collider.GetComponent<BaseInteractiveObject>().UniqueID == gameObject.GetComponent<BaseInteractiveObject>().UniqueID)
				{
					return true;
				}
			}
		}
		else if (Physics.Raycast( rayBottomLeft, out hit ))
		{
			if (hit.collider.GetComponent<BaseInteractiveObject>() != null)
			{
				if (hit.collider.GetComponent<BaseInteractiveObject>().UniqueID == gameObject.GetComponent<BaseInteractiveObject>().UniqueID)
				{
					return true;
				}
			}
		}
		else if (Physics.Raycast( rayBottomRight, out hit ))
		{
			if (hit.collider.GetComponent<BaseInteractiveObject>() != null)
			{
				if (hit.collider.GetComponent<BaseInteractiveObject>().UniqueID == gameObject.GetComponent<BaseInteractiveObject>().UniqueID)
				{
					return true;
				}
			}
		}

		return false;
	}

	protected virtual void OnUse()
	{
	}


	protected bool PlayerIsNearby()
	{
		return (Vector3.Distance (player.transform.position, transform.position) < InteractionRadius);
	}

	protected bool PlayerCanReachMe()
	{
		return (Vector3.Distance (player.transform.position, transform.position) < ActivationRadius);
	}

}

