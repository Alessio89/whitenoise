﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int starttime = System.Environment.TickCount;
		InitializeObjectsUID ();
		int endtime = System.Environment.TickCount;

		int totalTime = endtime - starttime;
		Debug.Log ("Took " + totalTime.ToString () + " ms.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InitializeObjectsUID()
	{
		Transform[] objs = GetAllInteractiveObjects ();
		for (int i = 0; i < objs.Length; i++)
		{
			if (objs[i].gameObject.GetComponent<BaseInteractiveObject>() != null)
			{
				objs[i].gameObject.GetComponent<BaseInteractiveObject>().UniqueID = "INTOBJ_" + i.ToString();
			}
		}
	}

	protected Transform[] GetAllInteractiveObjects()
	{
		Transform[] objs = FindObjectsOfType (typeof(Transform)) as Transform[];//(from o in FindObjectsOfType(typeof(MonoBehaviour)) where (o as GameObject).GetComponent<BaseInteractiveObject_Simple> () != null select (o as GameObject)).ToArray ();
		return objs;
	}
}
