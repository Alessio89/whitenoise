﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class DigitalClock : MonoBehaviour {

	Dictionary<string, GameObject> Pieces = new Dictionary<string, GameObject>();
	public int Numero = 0;
	

	// Use this for initialization
	void Start () {
		foreach( Transform t in GetComponentsInChildren<Transform>() )
		{
			if (t == transform)
				continue;
			Pieces[ t.gameObject.name ] = t.gameObject;
			Debug.Log (t.gameObject.name);
			t.gameObject.renderer.material.color = Color.black;
		}

		//ShowNumber (3);
	}
	
	// Update is called once per frame
	int count = 0;
	float timer = 0;
	void Update () {
		ShowNumber (Numero);
	}

	void ShowNumber( int number )
	{
		if (number > 9)
			return;
		foreach(KeyValuePair<string, GameObject> entry in Pieces)
		{
			entry.Value.renderer.material.color = new Color(0.36f, 0.36f, 0.36f, 1);
		}

		switch(number)
		{
		case 0:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopLeft"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["BottomLeft"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			Pieces["BottomCenter"].renderer.material.color = Color.red;
			break;
		case 1:
			Pieces ["TopRight"].renderer.material.color = Color.red;
			Pieces ["BottomRight"].renderer.material.color = Color.red;
			break;
		case 2:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			Pieces["BottomLeft"].renderer.material.color = Color.red;
			Pieces["BottomCenter"].renderer.material.color = Color.red;
			break;
		case 3:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			Pieces["BottomCenter"].renderer.material.color = Color.red;
			break;
		case 4:
			Pieces["TopLeft"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			break;
		case 5:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopLeft"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			Pieces["BottomCenter"].renderer.material.color = Color.red;
			break;
		case 6:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopLeft"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			Pieces["BottomCenter"].renderer.material.color = Color.red;
			Pieces["BottomLeft"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			break;
		case 7:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			break;
		case 8:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopLeft"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["BottomLeft"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			Pieces["BottomCenter"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			break;
		case 9:
			Pieces["TopCenter"].renderer.material.color = Color.red;
			Pieces["TopRight"].renderer.material.color = Color.red;
			Pieces["TopLeft"].renderer.material.color = Color.red;
			Pieces["MiddleCenter"].renderer.material.color = Color.red;
			Pieces["BottomRight"].renderer.material.color = Color.red;
			break;
		}
	}

}
