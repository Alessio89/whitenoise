﻿using UnityEngine;
using System.Collections;

public class Orologio : BaseMessageObject {
	GameObject ore;
	GameObject minuti;
	float changeTimer = 0;
	bool changed = false;
	int state = 0;
	
	protected override void Initialize ()
	{
		base.Initialize ();
		//ore = Helpers.GetChildByName ("ore", gameObject);
		ore = GameObject.Find ("ore");
		minuti = GameObject.Find ("minuti");
		Message = new string[] { "Speriamo non ci voglia troppo..." };
	}
	// Update is called once per frame
	protected override void UpdateLogic ()
	{
		base.UpdateLogic ();

		if (!ore.GetComponentInChildren<Renderer>().isVisible && !changed)
		{
			changeTimer += Time.deltaTime;
			if (changeTimer > 1.8f)
			{
				changed = true;
				Vector3 actualRot = ore.transform.localEulerAngles;
				actualRot.z += Random.Range (-100, 100);
				ore.transform.localEulerAngles = actualRot;

				minuti.transform.Rotate( new Vector3(0, 0, Random.Range(-100, 100)) );
				changeTimer = 0;
			}
		}
		else if (ore.GetComponentInChildren<Renderer>().isVisible)
			changed = false;
	}

	protected override void OnUse ()
	{
		base.OnUse ();
		switch(state)
		{
		case 0:
			Message = new string[] { "Ma... devo aver letto male" };
			state = 1;
			break;
		case 1:
			Message = new string[] { "Uhm...!" };
			state = 2;
			break;

		}
	}
}
