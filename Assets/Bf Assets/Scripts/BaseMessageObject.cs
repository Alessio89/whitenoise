﻿using UnityEngine;
using System.Collections;

public class BaseMessageObject : BaseInteractiveObject {

	public string[] Message;
	protected int mIndex = 0;
	protected bool showGui = false;
	public float MessageDelay = 0.03f;
	public bool DisablePlayer = true;

	public bool ReadFromFile = false;
	public string TextFileName = "";

	public bool UsedAtLeastOnce = false;

	protected override void Initialize ()
	{
		base.Initialize ();
		if (ReadFromFile)
			ParseTextFile ();
	}

	protected override void OnUse ()
	{
		Dialogue.ShowMessage (Message, MessageDelay, DisablePlayer);
		if (!UsedAtLeastOnce)
			UsedAtLeastOnce = true;
	}

	protected void ParseTextFile()
	{
		TextAsset t = Resources.Load ("BF Resources/Messages/" + TextFileName) as TextAsset;
		Message = t.text.Split (new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
	}

}
