﻿using UnityEngine;
using System.Collections;

public class Speaker : BaseMessageObject {
	protected override void OnUse ()
	{
		base.OnUse ();
		Dialogue.AddActionOnTextAppear ("Speaker:", () => {
			GetComponentInChildren<LedSpeaker> ().ActivateLed ();		
						 });
		Dialogue.AddActionOnTextAppear ("Grazie\"", () => {
			GetComponentInChildren<LedSpeaker> ().DisableLed ();		
				});
	}

	IEnumerator EnableLed()
	{
		while (!Dialogue.managerObject.currentMessage.Contains("Speaker:")) {
			yield return new WaitForSeconds (0.2f);
		}
		
		while (!Dialogue.managerObject.currentMessage.Contains("Grazie\"")) {
			yield return new WaitForSeconds(0.2f);
		}
		yield return new WaitForSeconds(1f);
		
	}
}
