﻿using UnityEngine;
using System.Collections;

public class TicketMachine : BaseInteractiveObject {

	Vector3 lastCameraPosition;
	bool looking = false;
	bool animating = false;
	GameObject ticketClone;
	InteractiveText text;
	bool used = false;

	protected override void OnUse ()
	{
		if (!used)
		{
			if (!looking)
			{
				player.GetComponent<PlayerManager>().DisablePlayer();
				animation.Play("Take 001");
				ticketClone = Instantiate( GameObject.Find("pPlane1").gameObject ) as GameObject;
				iTween.MoveTo( ticketClone, iTween.Hash( "position", Camera.main.transform.position + Camera.main.transform.forward * 0.2f,
				                                         "oncomplete", "RemoveTicketShowGui",
				                                        "oncompletetarget", gameObject,
				                                        "easetype", "easeOutSine",
				                                        "time", 0.7f,
				                                        "onstart", "SpawnTicketClone",
				                                        "onstarttarget", gameObject,
				                                        "delay", 1.1f));
				looking = true;
				used = true;

				GameObject.Find("Porta1").GetComponent<BaseMessageObject>().Message = new string[] { "Signor White, deve avere ancora un po' di pazienza, il prossimo è lei." };
				GameObject.Find("Speaker").GetComponent<BaseMessageObject>().Message = new string[] { "Signor White, deve avere ancora un po' di pazienza, il prossimo è lei." };
				
				if (GameObject.Find("[S] GFManager").GetComponent<GameflowManager>().S1_TimerIsRunning)
				{
					GameObject.Find("[S] GFManager").GetComponent<GameflowManager>().S1_StopTimer();

				}
			}
		}
		else
		{
			Dialogue.ShowMessage( new string[] { "Ho già preso un biglietto. Devo attendere il mio turno." } );
		}

	}

	void SpawnTicketClone()
	{
		Destroy (GameObject.Find ("pPlane1"));
		ticketClone.transform.position = GameObject.Find("pPlane1").transform.position;
		ticketClone.transform.up = -Camera.main.transform.forward;
		ticketClone.transform.Rotate( new Vector3(0, 90, 0) );
	}

	void RemoveTicketShowGui()
	{
		Destroy (ticketClone);
		GameObject.Find ("[S] PlayerTextureGui").GetComponent<GUITexture> ().enabled = true;
		GameObject.Find ("[S] PlayerTextureGui").GetComponent<GUITexture> ().texture = Resources.Load ("BF Resources/TicketGuiTexture") as Texture2D;
		GameObject.Find ("[S] PlayerTextureGui").GetComponent<GUITexture> ().transform.position = new Vector3 (0.4f, 0.1f, 0);
		Rect inset = GameObject.Find ("[S] PlayerTextureGui").GetComponent<GUITexture> ().pixelInset;
		inset.width = 300;
		inset.height = 500;

		GameObject.Find ("[S] PlayerTextureGui").GetComponent<GUITexture> ().pixelInset = inset;

		Dialogue.ShowMessage (new string[] {
						"42.",
						"Mi dice qualcosa, ma non riesco a focalizzarmi.",
						"Non sopporto l'attesa, mi sembra di stare in un limbo."
				}, 0.03f, true, MessagePosition.Top);
		Dialogue.managerObject.OnMessageEnd = () => { 
						player.GetComponent<PlayerManager> ().EnablePlayer ();
						Destroy (ticketClone);
						GameObject.Find ("[S] PlayerTextureGui").GetComponent<GUITexture> ().enabled = false;
						looking = false;
				};



	}

	protected override void UpdateLogic ()
	{
		base.UpdateLogic ();
	} 

	protected override void Initialize ()
	{
		base.Initialize ();
	//	GetComponent<Animation> ().animation.Stop ();
		text = TextManager.Create ("BigliettoRitira", GameObject.Find ("pPlane1"), "[F] - Ritira", false);
		text.gameObject.renderer.enabled = false;
		text.gameObject.transform.localScale *= 0.1f;
		text.TextColor = new Color (0, 0, 0, 1);
	}

	void OnGUI()
	{
		float offset = 10;
		GUI.Label( new Rect( Screen.width/2, Screen.height/2, 20, 20 ), "+");
		GUI.Label( new Rect( (Screen.width/2) - offset, (Screen.height/2) - offset, 20, 20 ), "+");
		GUI.Label( new Rect( (Screen.width/2) + offset, (Screen.height/2) - offset, 20, 20 ), "+");
		GUI.Label( new Rect( (Screen.width/2) - offset, (Screen.height/2) + offset, 20, 20 ), "+");
		GUI.Label( new Rect( (Screen.width/2) + offset, (Screen.height/2) + offset, 20, 20 ), "+");
	}

}
