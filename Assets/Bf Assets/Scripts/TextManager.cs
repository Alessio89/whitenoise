﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class TextManager 
{
	public static string InteractiveTextTag = "IntText";
	public static List<InteractiveText> TextList = new List<InteractiveText> ();

	public static InteractiveText Create(string name, GameObject link, string text, bool ActionText)
	{
		InteractiveText newText = new InteractiveText (name, text, false, link, ActionText);

		TextList.Add (newText);

		return newText;
	}

	public static void Destroy(string name)
	{
		InteractiveText queryResult = (from iT in TextList where iT.Name == name select iT).First ();
		if (queryResult == null)
			return;

		queryResult.Destroy ();
		TextList.Remove (queryResult);
	}

	public static InteractiveText GetTextObject(string name)
	{
		InteractiveText queryResult = (from iT in TextList where iT.Name == name select iT).First ();
		if (queryResult == null)
			return null;

		return queryResult;
	}

	public static void UpdatePositions()
	{
		foreach(InteractiveText t in TextList)
			t.UpdateTextPosition();
	}
}

public class InteractiveText
{
	public GameObject gameObject;
	public GameObject LinkedGameObject;
	public TextMesh Text;
	public InteractiveText ActionText = null;
	public string Name;

	public Color TextColor = Color.white;

	public Vector3 TextOffset;
	public float ActionTextOffset;
	public float FadingTime = 1;

	public bool ShowActionText = false;

	public bool Active = true;

	/* Constructors */

	public InteractiveText(string name, string text, bool fadeInOnCreate, GameObject link, bool actionText)
	{
		Name = name;
		gameObject = GameObject.Instantiate (GameObject.FindGameObjectWithTag (TextManager.InteractiveTextTag)) as GameObject;
		Text = gameObject.GetComponent<TextMesh> ();

		LinkedGameObject = link;

		if (gameObject == null || Text == null)
		{
			Debug.LogError("Errore: Impossibile inizializzare il testo interattivo. Il gameObject e/o il Text non sono stati creati.");
		}

		Text.text = text;
		gameObject.renderer.material.color = new Color (TextColor.r, TextColor.g, TextColor.b, 0);
		gameObject.transform.position = LinkedGameObject.transform.position;
		gameObject.renderer.enabled = true;

		if (fadeInOnCreate)
			FadeIn();
		else
			gameObject.renderer.enabled = false;

		if (actionText)
		{
			ActionText = TextManager.Create( Name + "_action", LinkedGameObject, "[F] Usa", false);
			ActionText.gameObject.transform.localScale *= 0.4f;
			ActionText.gameObject.renderer.material.color = new Color (TextColor.r, TextColor.g, TextColor.b, 0);
			
		}
	}
	
	public InteractiveText() : this ("Default", "Hello World", true, null, true) {}

	/* ------------ */

	/* Destroyers */

	public void Destroy()
	{
		GameObject.Destroy (gameObject);
	}

	/* ---------- */

	public void FadeIn()
	{
		gameObject.renderer.enabled = true;
		gameObject.renderer.material.color = new Color (TextColor.r, TextColor.g, TextColor.b, 0);
		iTween.ColorTo (gameObject, iTween.Hash ("color", new Color (TextColor.r, TextColor.g, TextColor.b, 1),
		                                              "time", FadingTime,
		                                              "easetype", "easeInOutSine"
		                                              ));


	}

	public void FadeOut()
	{
		iTween.ColorTo (gameObject, iTween.Hash ("color", new Color (TextColor.r, TextColor.g, TextColor.b, 0),
		                                   "time",FadingTime,
		                                   "easetype", "easeInOutSine"
		                                   ));

		if (ActionText != null)
		{
			ActionText.FadeOut();
		}
	}

	public void Enable()
	{
		Active = true;
		//FadeOut ();
		
		if (ActionText != null)
		{
			ActionText.Active = true;
		}
		
	}

	public void Disable()
	{
		Active = false;
		FadeOut ();

		if (ActionText != null)
		{
			ActionText.Active = false;
		}

	}

	public void UpdateTextPosition()
	{
		if (!Active)
			return;
		gameObject.transform.position = LinkedGameObject.transform.position;
		gameObject.transform.Translate (TextOffset, Space.World);														
		
		gameObject.transform.LookAt(Camera.main.transform.position);
		// It seems that we have to rotate 180 degrees on the Y axis or the text will be mirrored (not sure why?)
		gameObject.transform.Rotate(0, 180, 0);

		if (ActionText != null)
		{
			ActionText.gameObject.transform.position = gameObject.transform.position + new Vector3(0, ActionTextOffset, 0);
			//ActionText.gameObject.transform.Translate( new Vector3(0, 0, ActionTextOffset), Space.World );//new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + ActionTextOffset, gameObject.transform.position.z);
			ActionText.gameObject.transform.LookAt(Camera.main.transform.position);
			ActionText.gameObject.transform.Rotate(0, 180, 0);
		}

		if (Dialogue.IsMessageShowing())
		{
			gameObject.renderer.enabled = false;
			if (ActionText != null)
			{
				ActionText.gameObject.renderer.enabled = false;
			}
		}
		else
		{
			gameObject.renderer.enabled = true;
			if (ActionText != null)
			{
				ActionText.gameObject.renderer.enabled = true;
			}
		}
	}
}
