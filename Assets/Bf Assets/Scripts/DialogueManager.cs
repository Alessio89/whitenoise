﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MessagePosition
{
	Bottom = 0,
	Top
}

public class DialogueManager : MonoBehaviour {

	public GUIStyle MessageStyle;
	public string[] Messages;
	public int mIndex = 0;
	public string currentMessage = "";
	public Rect TextPosition;
	public MessagePosition Position = MessagePosition.Bottom;
	public bool AutomaticForward = false;

	public System.Action OnMessageEnd;

	// Use this for initialization
	void Start () {
		Dialogue.Initialize (this);
		StartCoroutine (CheckOnTextAppear ());
	}

	public bool ShowMessage = false;
	public bool OverrideMessage = true;
	public bool DisablePlayer = true;
	
	// Update is called once per frame
	void Update () {
		if (ShowMessage)
		{

			switch (Position)
			{
			case MessagePosition.Bottom:
				TextPosition = new Rect (Screen.width / 2 - (Screen.width/4), Screen.height - (Screen.height/5), Screen.width / 2, 100);
				break;
			case MessagePosition.Top:
				TextPosition = new Rect (Screen.width / 2 - (Screen.width/4), 20, Screen.width / 2, 100);
				break;
			}

			if (!AutomaticForward)
			{
				if (Input.GetKeyDown(KeyCode.Space))
				{
					if (currentIndex != Messages[mIndex].Length)
					{
						return;
					}

					for(int i = 0; i < Dialogue.OnTextAppear.Count; i++)
					{
						OnTextAppearData data = Dialogue.OnTextAppear[i];
						if (Messages[mIndex].Contains( data.Text ))
						{
							data.OnAppear();
							Dialogue.OnTextAppear.Remove(data);
							i--;
						}
					}
					// Check if something has to happen in this message


					currentIndex = 0;
					currentMessage = "";
					mIndex++;
					if (mIndex >= Messages.Length)
					{
						mIndex = 0;
						ShowMessage = false;
						if (DisablePlayer)
							GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>().EnablePlayer();
						if (OnMessageEnd != null)
						{
							OnMessageEnd();
							OnMessageEnd = null;
						}
					}
					else
					{
						StartCoroutine( TypeMessage() );
					}
				}
			}
	
		}
	}
	float messageDelay = 0.01f;
	public void StartMessage(float speed = 0.01f)
	{
		currentIndex = 0;
		mIndex = 0;
		currentMessage = "";
		if (DisablePlayer && GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterController>().enabled  )
			GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>().DisablePlayer();
		messageDelay = speed;
		StartCoroutine (TypeMessage ());
	}

	void OnGUI()
	{
		if (!ShowMessage)
			return;

		GUI.Label (TextPosition, currentMessage, MessageStyle);
	}
	public int currentIndex = 0;

	public IEnumerator CheckOnTextAppear()
	{
		for(int i = 0; i < Dialogue.OnTextAppear.Count; i++)
		{
			OnTextAppearData data = Dialogue.OnTextAppear[i];
			if (currentMessage.Contains( data.Text ) && !data.Done)
			{
				data.OnAppear();
				Dialogue.OnTextAppear.Remove( data );
			}
		}

		yield return new WaitForSeconds (0.5f);
		StartCoroutine (CheckOnTextAppear ());
	}

	public IEnumerator TypeMessage()
	{
		//currentMessage = "";
		
		while(currentIndex < Messages[mIndex].Length)
		{
			currentMessage += Messages[mIndex][currentIndex];
			currentIndex++;
			yield return new WaitForSeconds(messageDelay);
		}

		if (AutomaticForward && ShowMessage)
		{
			yield return new WaitForSeconds (2);
			mIndex++;
			
			if (mIndex >= Messages.Length)
			{
				//mIndex = 0;
				ShowMessage = false;
				if (OnMessageEnd != null)
				{
					OnMessageEnd();
					OnMessageEnd = null;
				}
				return false;

			}
			
			currentIndex = 0;
			currentMessage = "";

			StartCoroutine (TypeMessage ());
		}
	}
}

public static class Dialogue {
	public static DialogueManager managerObject;
	public static AutomaticDialogue automaticManagerObject;
	public static List<OnTextAppearData> OnTextAppear;

	public static bool IsMessageShowing()
	{
		return managerObject.ShowMessage;
	}

	public static void Initialize(DialogueManager dManager)
	{
		managerObject = dManager;
		OnTextAppear = new List<OnTextAppearData> ();
	}

	public static void ShowMessage(string[] Message, float speed = 0.03f, bool DisablePlayer = true, MessagePosition textPosition = MessagePosition.Bottom, bool automaticSwitch = false, bool overrideMessage = true)
	{
		if (automaticSwitch)
		{
			automaticManagerObject.Position = textPosition;
			automaticManagerObject.ShowMessage = true;
			automaticManagerObject.Messages = Message;
			automaticManagerObject.DisablePlayer = DisablePlayer;
			automaticManagerObject.OverrideMessage = overrideMessage;
			automaticManagerObject.StartMessage (speed);
		}
		else
		{
			managerObject.Position = textPosition;
			managerObject.ShowMessage = true;
			managerObject.Messages = Message;
			managerObject.DisablePlayer = DisablePlayer;
			managerObject.OverrideMessage = overrideMessage;
			managerObject.StartMessage (speed);
		}
	}

	public static void StopMessage()
	{
		managerObject.ShowMessage = false;
	}

	public static void StopAutomaticMessage()
	{
		automaticManagerObject.ShowMessage = false;
	}

	public static void AddActionOnTextAppear( string text, System.Action action)
	{
		OnTextAppearData data = new OnTextAppearData ();
		data.DoOnce = true;
		data.Done = false;
		data.Text = text;
		data.OnAppear = action;
		OnTextAppear.Add (data);
	}

}

public struct OnTextAppearData
{
	public bool DoOnce;
	public System.Action OnAppear;
	public string Text;
	public bool Done;
}
