﻿using UnityEngine;
using System.Collections;

public class vetro : BaseMessageObject 
{
	int mIndex = 0;
	int currentIndex = 0;
	string currentMessage = "";

	bool started = false;
	protected override void UpdateLogic ()
	{
		base.UpdateLogic ();

		IntText.ActionText.Disable ();

		if (PlayerIsNearby() && !started)
		{
			started = true;
			if (!UsedAtLeastOnce)
				UsedAtLeastOnce = true;
			Dialogue.ShowMessage( Message, MessageDelay, DisablePlayer, MessagePosition.Top , true);

			if (Dialogue.automaticManagerObject.mIndex >= Message.Length - 1)
			{
				mIndex = 0;
				Dialogue.automaticManagerObject.mIndex = 0;
			}

			if (mIndex != 0)
			{
				Dialogue.automaticManagerObject.mIndex = mIndex;
			}

			if (currentIndex != 0)
			{
				Dialogue.automaticManagerObject.currentIndex = currentIndex;
			}
			if (currentMessage != "")
			{
				Dialogue.automaticManagerObject.currentMessage = currentMessage;
			}
		}
		else if (!PlayerIsNearby() && started)
		{
			if (Dialogue.automaticManagerObject.mIndex >= Message.Length - 1)
			{
				mIndex = 0;
				Dialogue.automaticManagerObject.mIndex = 0;
				Dialogue.automaticManagerObject.currentMessage = "";
				currentMessage = "";
				currentIndex = 0;
			}

			if (Dialogue.automaticManagerObject.currentIndex == Message[Dialogue.automaticManagerObject.mIndex].Length - 1)
			{
				currentIndex = 0;
				currentMessage = "";
			}
			else
			{
				currentIndex = Dialogue.automaticManagerObject.currentIndex;
				currentMessage = Dialogue.automaticManagerObject.currentMessage;
			}

			if (mIndex == Message.Length - 1)
				mIndex = 0;
			else
				mIndex = Dialogue.automaticManagerObject.mIndex;


			started = false;
			Dialogue.StopAutomaticMessage();
		}

	}

	protected override void OnUse ()
	{
		return;
	}

}
