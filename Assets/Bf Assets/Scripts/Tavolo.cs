﻿using UnityEngine;
using System.Collections;

public class Tavolo : BaseMessageObject {

	bool used = false;
	bool penTaken = false;
	bool disable = false;

	protected override void OnUse ()
	{
		if (disable)
		{
			return;
		}
		if (!used)
		{
		
			Dialogue.ShowMessage (Message, MessageDelay, DisablePlayer);
			Dialogue.managerObject.OnMessageEnd = () => {
				IntText.Text.text = "Penna";
				IntText.ActionText.Text.text = "[F - Prendi]";
			};
			used = true;
		}
		else
		{
			Destroy(GameObject.Find("penna"));
			disable = true;
			penTaken = true;
			IntText.Disable();
			
		}
	}


}
