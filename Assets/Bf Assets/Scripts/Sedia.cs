﻿using UnityEngine;
using System.Collections;

public class Sedia : BaseInteractiveObject {

	bool IsSitting = false;
	bool canStandUp = false;
	InteractiveText text;
	public Texture2D newspaperTexture;
	Vector3 lastPlayerPosition;


	IEnumerator CheckRoutine()
	{
		iTween.RotateBy( player, iTween.Hash("amount", new Vector3(0, 0.1f, 0), "easeType", "easeInOutSine", "time", 3));
		while(!Dialogue.automaticManagerObject.currentMessage.Contains("Deve essere una clinica."))
		{
			yield return new WaitForSeconds(0.2f);
		}
		iTween.RotateBy( player, iTween.Hash("amount", new Vector3(0, -0.2f, 0), "easeType", "easeInOutSine", "time", 6));
		while(!Dialogue.automaticManagerObject.currentMessage.Contains("Non ricordo..."))
		{
			yield return new WaitForSeconds(0.2f);
		}
		yield return new WaitForSeconds (3.2f);
		iTween.RotateBy( player, iTween.Hash("amount", new Vector3(0, 0.1f, 0), "easeType", "easeInOutSine", "time", 3));
		
	}


	protected override void OnUse ()
	{
		if (!IsSitting)
		{
			iTween.Stop( player );
			player.GetComponent<PlayerManager>().DisablePlayer();


			IsSitting = true;
		}
		lastPlayerPosition = player.transform.position;
		player.transform.position = transform.position - (transform.forward * 1.2f);// - transform.forward * 0.5f;
		Vector3 pos = player.transform.position;
		pos.y = 1.32f;
		player.transform.position = pos;
		LeanForward ();
	}

	void LeanForward()
	{
		iTween.MoveBy( player, iTween.Hash("amount", gameObject.transform.right * 0.2f, "time", 0.5f, "easeType", "easeOutSine", "oncomplete", "Rotate", "oncompletetarget", gameObject));
	}

	void Rotate()
	{
		iTween.RotateTo( player, iTween.Hash("rotation", new Vector3(0, 90, 0), "time", 1, "easeType", "easeOutSine", "oncomplete", "SitDown", "oncompletetarget", gameObject));
	}

	void SitDown()
	{
		iTween.MoveBy (player, iTween.Hash ("amount", -player.transform.up * 0.5f, "time", 0.5f, "easeType", "easeOutSine", "oncomplete", "LookForward", "oncompletetarget", gameObject));
	}

	void LookForward()
	{
		iTween.RotateTo (Camera.main.gameObject, iTween.Hash ("rotation", new Vector3 (0, 90, 0), "time", 1, "easeType", "easeOutSine", "oncomplete",(GameObject.Find("Giornale").renderer.enabled) ? "NewsPaper" : "EnableLook", "oncompletetarget", gameObject));
	}

	bool CanLook = false;

	void NewsPaper()
	{

		GameObject quad = GameObject.Find ("[S] PlayerImageGui");
		quad.renderer.material.mainTexture = newspaperTexture;
		quad.renderer.material.color = new Color (1, 1, 1, 1);
		quad.transform.localScale = new Vector3 (1.66f, 1.13f, 1);
		//quad.transform.position = new Vector3 (-0.07f, -0.004f,1.25f);
		quad.transform.Translate (new Vector3 (0, 0, 0.5f));
		Dialogue.ShowMessage (new string[] { "Leggiamo qualcosa per passare il tempo...", "[NOTA DESIGN: QUI ANDRA LA TEXTURE DEL GIORNALE CON LA NOTIZIA COME", "RIPORTATO NEL POST DEL GAMEFLOW." });
		Dialogue.managerObject.OnMessageEnd = () =>
				{
			player.GetComponent<PlayerManager>().DisablePlayer();
			iTween.MoveBy(quad, iTween.Hash("amount", new Vector3(-0.3f,0, -0.6f), "time", 1, "easetype", "easeInOutSine", "oncomplete", "HideQuad", "oncompletetarget", gameObject));
			//quad.renderer.material.color = new Color(1,1,1,0);
		};
	}

	void HideQuad()
	{
		StartCoroutine (HideQuadDelay ());
	}

	IEnumerator HideQuadDelay()
	{
		yield return new WaitForSeconds (4);
		GameObject quad = GameObject.Find ("[S] PlayerImageGui");
		quad.renderer.material.color = new Color (1, 1, 1, 0);
		player.transform.position = lastPlayerPosition;
		player.GetComponent<PlayerManager> ().EnablePlayer ();
	}

	void EnableLook()
	{
		CanLook = true;
	}

	void EnablePlayer()
	{
		//player.GetComponent<PlayerManager> ().EnablePlayer ();
		Dialogue.ShowMessage( new string[] { "Questa sedia è cosa più scomoda che io abbia mai visto. Ho ancora il sedere indolenzito." } );
		
	}


	float rotationX = 0;
	protected override void UpdateLogic ()
	{
		base.UpdateLogic ();

		if (Input.GetKeyDown(KeyCode.Space) && canStandUp)
		{
			IsSitting = false;
			player.GetComponent<PlayerManager> ().EnablePlayer ();
			Dialogue.StopAutomaticMessage();
			canStandUp = false;
			GameObject.Find("[S] MiscMessages").GetComponent<GUIText>().enabled = false;
			GameObject.Find("[S] MiscMessages").GetComponent<GUIText>().text = "";
		}

		if (CanLook)
		{
			rotationX += Input.GetAxis("Mouse X") * 2;
			rotationX = Mathf.Clamp(rotationX, -90, 90);
			Camera.main.transform.localEulerAngles = new Vector3(0, rotationX, 0);

			if (Input.GetKeyDown(KeyCode.Space))
			{
				CanLook = false;
				iTween.MoveBy (player, iTween.Hash ("amount", player.transform.up * 0.5f, "time", 0.5f, "easeType", "easeOutSine", "oncomplete", "EnablePlayer", "oncompletetarget", gameObject));
			//	player.GetComponent<PlayerManager>().EnablePlayer();
				IsSitting = false;
			}
		}
	}

}
