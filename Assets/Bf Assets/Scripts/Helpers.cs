﻿using UnityEngine;
using System.Collections;
using System.Linq;

public static class Helpers {

	public static GameObject GetChildByName( string name, GameObject parent )
	{
		GameObject obj;

		obj = (from o in parent.GetComponentsInChildren<Transform>() where obj.gameObject.name == name select obj.gameObject).First();

		return obj;
	}

}