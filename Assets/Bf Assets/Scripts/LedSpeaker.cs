﻿using UnityEngine;
using System.Collections;

public class LedSpeaker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		renderer.material.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void ActivateLed()
	{
		renderer.material.color = Color.green;
		GetComponentInChildren<Light> ().enabled = true;
	}
	public void DisableLed()
	{
		renderer.material.color = Color.red;
		GetComponentInChildren<Light> ().enabled = false;
	}
}
