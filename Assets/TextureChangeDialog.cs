﻿using UnityEngine;
using System.Collections;

public class TextureChangeDialog : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	bool showTextures = false;
	int selected = 0;

	void OnGUI()
	{
		if (GUI.Button( new Rect(Screen.width - 150, 10, 120, 20), "Cambia Texture"))
		{
			showTextures = !showTextures;
		}

		if (showTextures)
		{
			//GUILayout.SelectionGrid(0, new string[] { "Test1", "Test2" }, 2, "toggle");
			selected = GUI.SelectionGrid( new Rect( Screen.width - 150, 50, 140, 120), selected, new string[] { "Legno", "Marmo 1", "Marmo 2", "Pietra" }, 2);
		}

		if (selected == 0)
		{
			ChangeTexture("wood", new Vector2(1,1));
		}
		else if (selected == 1)
		{
			ChangeTexture("marble", new Vector2(10,10));
		}
		else if (selected == 2)
		{
			ChangeTexture("blackmarble", new Vector2(10,10));
		}
		else if (selected == 3)
		{
			ChangeTexture("stone", new Vector2(10,10));
		}
	}

	void ChangeTexture(string textureName, Vector2 tiling)
	{
		GameObject.Find("Parete1").renderer.sharedMaterial.SetTexture(0, Resources.Load("BF Resources/WallTextures/"+textureName) as Texture);
		GameObject.Find ("Parete1").renderer.sharedMaterial.mainTextureScale = tiling;
	}
}
